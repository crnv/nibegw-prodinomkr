**[NibeGW for ProDino MKR Zero Ethernet V1 boards](https://kmpelectronics.eu/products/prodino-mkr-zero-ethernet-v1/)**

## Installation

1. Follow [this tutorial](https://kmpelectronics.eu/tutorials-examples/prodino-mkr-versions-tutorial/) to setup Arduino Studio for the ProDino MKR Zero Ethernet board and install the required libraries.

2. Download all NibeGW files from [here](https://bitbucket.org/crnv/nibegw-prodinomkr/src/master/). Download Watchdog lib for MKR from [here](https://github.com/javos65/WDTZero). Install the libs in Arduino Studio.

3. Create a new Arduino project, adapt IP configuration to your needs and compile and upload NibeGW to the MKR Zero Board.